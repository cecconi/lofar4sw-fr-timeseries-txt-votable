#!/usr/bin/env python

from lxml import etree
from pathlib import Path
from astroquery.simbad import Simbad
from astropy.coordinates import SkyCoord
import astropy.units as u
#import requests
#from io import BytesIO

from collections import OrderedDict

HEADER_FIELD_NAMES = ['MJD', 'DM', 'DMErr', 'Chisq', 'RM', 'RMErr', 'RMiono', 'Freq', 'BW', 'Time', 'Tel']
FIELDS = OrderedDict([
    ('MJD', {
        "datatype": "double",
        "name": "MJD",
        "ucd": "time.epoch",
        "unit": "d",
        "ref": "time_frame",
        "_description": "Modified Julian Day (fractional) of the midpoint of the observation."
    }),
    ('DM', {
        "datatype": "double",
        "name": "DM",
        "ucd": "phys.dispMeasure",
        "unit": "pc.cm**-3",
        "_description": "Dispersion Measure of the pulsar estimated from the observation."
    }),
    ('DMErr', {
        "datatype": "float",
        "name": "DMErr",
        "ucd": "stat.error;phys.dispMeasure",
        "unit": "pc.cm**-3",
        "_description": "One sigma uncertainty on the measured DM."
     }),
    ('Chisq', {
        "datatype": "float",
        "name": "Chisq",
        "ucd": "stat.fit.chi2;phys.dispMeasure",
        "unit": "",
        "_description": "Reduced Chi square of the DM fit obtained from TEMPO2."
    }),
    ('RM', {
        "datatype": "float",
        "name": "RM",
        "ucd": "phys.polarization.rotMeasure",
        "unit": "rad.m**-2",
        "_description": "Rotation Measure estimated from the pulsar observation."
    }),
    ('RMErr', {
        "datatype": "float",
        "name": "RMErr",
        "ucd": "stat.error;phys.polarization.rotMeasure",
        "unit": "rad.m**-2",
        "_description": "Uncertainty on the measured RM."
    }),
    ('RMiono', {
        "datatype": "float",
        "name": "RMiono",
        "ucd": "phys.polarization.rotMeasure",
        "unit": "rad.m**-2",
        "_description": "The ionospheric RM at the time of the observation."
    }),
    ('Freq', {
        "datatype": "float",
        "name": "Freq",
        "ucd": "em.freq",
        "unit": "MHz",
        "_description": "Centre frequency of the observing band."
    }),
    ('BW', {
        "datatype": "float",
        "name": "BW",
        "ucd": "instr.bandwidth;em.freq",
        "unit": "MHz",
        "_description": "Spectral bandwidth of the observing band."
    }),
    ('Time', {
        "datatype": "float",
        "name": "Time",
        "ucd": "time.exposure",
        "unit": "min",
        "_description": "The total duration of the observation."
    }),
    ('Tel', {
        "datatype": "char",
        "arraysize": "*",
        "name": "Tel",
        "ucd": "meta.id;instr",
        "_description": "Data taking telescope or station."
    })
])


def get_element_field(field_id):
    """Returns an XML etree Element from a FIELDS dictionary (key, value) pair."""
    description = etree.Element("DESCRIPTION")
    description.text = FIELDS[field_id]['_description']
    field = etree.Element(
        "FIELD",
        ID=field_id,
        **{key: FIELDS[field_id][key] for key in FIELDS[field_id] if key[0] != '_'})
    field.append(description)
    return field


def get_element_coosys():
    """Returns an XML etree Element with the COOSYS metadata."""
    return etree.Element("COOSYS", ID="system", epoch="J2000", system="ICRS")


def get_element_timesys():
    """Returns an XML etree Element with the TIMESYS metadata."""
    return etree.Element("TIMESYS", ID="time_frame", refposition="TOPOCENTER",
                         timeorigin="2400000.5", timescale="GPS")


#def validate(votable: etree.Element):
#    xmlschema_url = 'http://www.ivoa.net/xml/VOTable/v1.3'
#    xmlschema_doc = etree.parse(BytesIO(requests.get(xmlschema_url).text.encode()))
#    xmlschema = etree.XMLSchema(xmlschema_doc)
#    return xmlschema.assert_(votable)


def get_element_table_params(source_name):
    """Returns a list of XML etree Elements with the Table Params"""
    params = list()
    params.append(
        etree.Element("PARAM", value=source_name, datatype="char", arraysize="*",
                      ID="SRC", name="Source Name", ucd="meta.id;src")
    )
    simbad_result = Simbad.query_object(source_name)
    coords = SkyCoord(
        f"{simbad_result['RA'][0]} {simbad_result['DEC'][0]}",
        unit=(u.hourangle, u.deg),
    )
    params.append(
        etree.Element("PARAM", value=str(coords.ra.value), datatype="float",
                      ID="RA", name="RA", ucd="pos.eq.ra", ref="system")
    )

    params.append(
        etree.Element("PARAM", value=str(coords.dec.value), datatype="float",
                      ID="Dec", name="Dec", ucd="pos.eq.dec", ref="system")
    )
    return params


def load_data(content):
    """Loads data from provided iterator."""
    for i, row in enumerate(content):
        if i == 0:
            yield row.strip().split()[1:]
            # This should be ("MJD", "DM", "DMErr", "Chisq", "RM", "RMErr", "RMiono", "Freq", "BW", "Time", "Tel")
        else:
            yield row.strip().split()


def convert(csv_file):

    with open(csv_file, 'r') as f:
        raw_data = load_data(f.readlines())
    header = next(raw_data)
    if HEADER_FIELD_NAMES != header:
        print(header)
        raise ValueError("wrong header format")

    source_name = f"PSR {csv_file.stem.split('_')[0]}"

    # preparing VOTable 1.4 header section
    xmlns = 'http://www.ivoa.net/xml/VOTable/v1.3'
    xsi = 'http://www.w3.org/2001/XMLSchema-instance'
    schemaLocation = 'http://www.ivoa.net/xml/VOTable/v1.3 http://www.ivoa.net/xml/VOTable/v1.3'
    version = '1.4'
    votable = etree.Element("VOTABLE", version=version, nsmap={None: xmlns, 'xsi': xsi})
    votable.set("{" + xsi + "}schemaLocation", schemaLocation)

    # <VOTABLE><RESOURCE>
    resource = etree.Element("RESOURCE")

    # <VOTABLE><RESOURCE><DESCRIPTION>
    description = etree.Element("DESCRIPTION")
    description.text = "LOFAR4SW DM-RM Timeseries"
    resource.append(description)

    # <VOTABLE><RESOURCE><COOSYS>
    resource.append(get_element_coosys())

    # <VOTABLE><RESOURCE><TIMESYS>
    resource.append(get_element_timesys())

    # <VOTABLE><RESOURCE><TABLE>
    table = etree.Element("TABLE", name=f"{source_name} DM-RM timeseries", nrows="")

    # <VOTABLE><RESOURCE><TABLE><PARAM>
    for item in get_element_table_params(source_name):
        table.append(item)

    # <VOTABLE><RESOURCE><TABLE><FIELD>
    for field_id in HEADER_FIELD_NAMES:
        table.append(get_element_field(field_id))

    # <VOTABLE><RESOURCE><TABLE><DATA><TABLEDATA>
    data = etree.Element("DATA")
    table_data = etree.Element("TABLEDATA")

    # <VOTABLE><RESOURCE><TABLE><DATA><TABLEDATA><TR><TD>
    nrows = 0
    for row in raw_data:
        tr = etree.Element("TR")
        for item in row:
            td = etree.Element("TD")
            td.text = item
            tr.append(td)
        table_data.append(tr)
        nrows += 1

    data.append(table_data)
    table.append(data)
    table.set("nrows", str(nrows))
    resource.append(table)
    votable.append(resource)

    votable_file = csv_file.parent / 'output.xml'
    with open(votable_file, 'wb') as f:
        f.write(etree.tostring(votable, xml_declaration=True, encoding="utf-8"))


if __name__ == "__main__":
    import sys
    argv = sys.argv[1:]

    csv_file = Path(argv[0])
    if csv_file.exists():
        convert(csv_file)
    else:
        raise FileNotFoundError(csv_file)
