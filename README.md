# LOFAR4SW Faraday Rotation Timeseries conversion script    

This python script converts the LOFAR4SW Faradar-Rotation text formatted 
timeseries file into an XML VOTable, adding the relevant metadata on the fly.

## Example

```
python convert.py examples/J0332+5434_DM-RM_timeseries.txt
```

This command produces a file called `output.xml` and should have the 
same content as `examples/J0332+5434_DM-RM_timeseries.xml`.
